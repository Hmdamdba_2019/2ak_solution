package HauptClasse;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLiteJDBCDriverConnection {
	static boolean zugriff = false;
	public static Connection conn = null;
	public static Statement DBStmt = null;
	static String stmt = "";
	static Statement hstmt = null;
	static String wd = System.getProperty("user.dir");
	static String DateiName = String.format("%s/Connection.txt",wd);
	static String pfad = "C:/SqLite/sweetkalnder.db";
	public String getConnectionPfad(String datName)
	{
		String ret = "";
		File file = new File(datName); 
		if (!file.canRead() || !file.isFile())
        {
        	System.out.println("Kann nicht das File "+file+" Finden");
            System.exit(0); 
        }
            BufferedReader in = null; 
        try { 
            in = new BufferedReader(new FileReader(datName)); 
            String zeile = null;
            while ((zeile = in.readLine()) != null) { 
            	ret = zeile;
            } 
        } catch (IOException e) { 
            e.printStackTrace(); 
        } finally { 
            if (in != null) 
                try { 
                    in.close(); 
                } catch (IOException e) { 
                } 
        } 
        return ret;
	}
	public void SetStmt (String toSet)
	{
		if(zugriff = false)
			System.out.println("Kein Zugrif auf die Datenbank!");
		else
			stmt = toSet;
	}
	public String GetStmt(){
		return stmt;
	}
    public boolean GetStatus(){
    	return zugriff;
    }
    public Connection GetConnect(){
    	return conn;
    }
    public Statement GetDBStmt()
    {
    	return DBStmt;
    }
	/**
     * Connect to a sample database
     */
    public void connect() {
        try {
            // db parameters
        	String tmp1 = getConnectionPfad(DateiName);
        	String url = String.format("jdbc:sqlite:%s", (tmp1.length() > 0)?tmp1:pfad);
            // create a connection to the database
            conn = DriverManager.getConnection(url);
            zugriff = true;
            DBStmt = conn.createStatement();
            // System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
        finally {/**
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }*/
        }
    }
}
/**
List<String> Values = new ArrayList<String>();
List<String> lids = new ArrayList<String>();

String query = "SELECT rlink_id, COUNT(*)"
             + "FROM dbo.Locate  "
             + "GROUP BY rlink_id ";

Statement stmt = yourconnection.createStatement();
try {
    ResultSet rs4 = stmt.executeQuery(Stmt);

    while (rs4.next()) {
        Values.add(rs4.getString(1));
        //lids.add(rs4.getString(2));
    }
} finally {
    stmt.close();
}

String show[] = sids.toArray(sids.size());
String actuate[] = lids.toArray(lids.size());
*/
